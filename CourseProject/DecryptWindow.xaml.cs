﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Text;
using System.Windows;

namespace CourseProject
{
    /// <summary>
    /// Interaction logic for DecryptWindow.xaml
    /// </summary>
    public partial class DecryptWindow : Window
    {
        private MainWindow m_parent;
        protected string dataPath;
        protected string res;

        public DecryptWindow()
        {
            InitializeComponent();
            keyEnter.Visibility = Visibility.Collapsed;
            keyPlease.Visibility = Visibility.Collapsed;
            resText.Visibility = Visibility.Collapsed;
            currentFile.Visibility = Visibility.Collapsed;
            btnDecrypt.Visibility = Visibility.Collapsed;
            btnSave.Visibility = Visibility.Collapsed;
        }

        public DecryptWindow(MainWindow parent) : this()
        {
            m_parent = parent;
        }

        public void clickSelectFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Text files (*.txt)|*.txt";
            dialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            if (dialog.ShowDialog() == true)
            {
                dataPath = dialog.FileName;
                keyEnter.Visibility = Visibility.Visible;
                keyPlease.Visibility = Visibility.Visible;
                btnDecrypt.Visibility = Visibility.Visible;
                currentFile.Visibility = Visibility.Visible;
                currentFile.Text = $"Вы работаете с файлом:\n{dialog.FileName}";
            }
        }

        private void decryptClick(object sender, RoutedEventArgs e)
        {
            string key = keyEnter.Text.Trim().ToLower();
            if (String.IsNullOrEmpty(key))
            {
                MessageBox.Show("Введите ключ шифрования!");
                return;
            }
            foreach (var c in key.ToLower())
            {
                if (!CourseProject.Operations.alphabet.Contains(c.ToString()))
                {
                    MessageBox.Show("Введите ключ в соответствии с правилами!!!");
                    return;
                }
            }
            string data = File.ReadAllText(dataPath, Encoding.GetEncoding(0));
            res = Operations.Vigenere(data, key, Operations.Operation.Decrypt);
            resText.Visibility = Visibility.Visible;
            btnSave.Visibility = Visibility.Visible;
            resText.Text = res;
        }

        private void clickSave(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            if (saveFileDialog1.ShowDialog() == true)
            {
                File.WriteAllText(saveFileDialog1.FileName, res);
                MessageBox.Show($"Дешифрованный файл сохранен по адресу:\n{saveFileDialog1.FileName}");
            }
        }

        private void decryptClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            m_parent.helloText.Text = File.ReadAllText(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Phrases.txt")).Split(';')[1];
            m_parent.Show();
        }

        private void backClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
