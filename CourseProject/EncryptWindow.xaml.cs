﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;

namespace CourseProject
{
    /// <summary>
    /// Interaction logic for EncryptWindow.xaml
    /// </summary>
    public partial class EncryptWindow : Window
    {
        private MainWindow m_parent;
        private string placeholder = "Введите текст, который хотите зашифровать шифром Виженера. После нажатия кнопки Зашифровать в окне справа появится результат.";
        protected string res;
        bool hasRealText = false;
        public EncryptWindow()
        {
            InitializeComponent();
            initialText.Text = placeholder;
            initialText.Foreground = Brushes.Silver;
        }

        public EncryptWindow(MainWindow parent):this()
        {
            m_parent = parent;
        }

        private void backClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void closeEncrypt(object sender, System.ComponentModel.CancelEventArgs e)
        {
            m_parent.helloText.Text = File.ReadAllText(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Phrases.txt")).Split(';')[2];
            m_parent.Show();
        }

        private void clickSave(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            if (saveFileDialog1.ShowDialog() == true)
            {
                File.WriteAllText(saveFileDialog1.FileName, res);
                MessageBox.Show($"Зашифрованный файл сохранен по адресу:\n{saveFileDialog1.FileName}");
            }
        }

        private void OnGotFocus(object sender, RoutedEventArgs e)
        {
            if (initialText.Text.Equals(placeholder) && !hasRealText)
            {
                initialText.Foreground = Brushes.Black;
                initialText.Text = string.Empty;
            }
        }

        private void OnLostFocus(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(initialText.Text))
            {
                initialText.Text = placeholder;
                initialText.Foreground = Brushes.Silver;
                hasRealText = false;
            }
            else
            {
                hasRealText = true;
            }
        }

        private void clickEncrypt(object sender, RoutedEventArgs e)
        {
            string key = keyEnter.Text.Trim().ToLower();
            if (String.IsNullOrEmpty(key))
            {
                MessageBox.Show("Введите ключ шифрования!");
                return;
            }
            foreach (var c in key.ToLower())
            {
                if (!Operations.alphabet.Contains(c.ToString()))
                {
                    MessageBox.Show("Введите ключ в соответствии с правилами!!!");
                    return;
                }
            }
            res = Operations.Vigenere(initialText.Text == placeholder && !hasRealText ? "" : initialText.Text, key, Operations.Operation.Encrypt);
            resText.Text = res;
        }

        private void keyPressed(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    initialText.Text += Environment.NewLine;
                    initialText.SelectionStart = initialText.Text.Length;
                    break;
                default:
                    break;
            }
        }
    }
}
