﻿using System;
using System.IO;
using System.Windows;

namespace CourseProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public int phrase = 0;
        public MainWindow()
        {
            InitializeComponent();
            var helloString = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Phrases.txt")).Split(';')[0];
            helloText.Text = helloString;
        }

        private void clickDecrypt(object sender, RoutedEventArgs e)
        {
            DecryptWindow decryptWindow = new DecryptWindow(this);
            decryptWindow.Show();
            Hide();
        }

        private void clickEncrypt(object sender, RoutedEventArgs e)
        {
            EncryptWindow encryptWindow = new EncryptWindow(this);
            encryptWindow.Show();
            Hide();
        }
    }
}
