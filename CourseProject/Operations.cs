﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseProject
{
    public class Operations
    {
        public enum Operation
        {
            Encrypt = 1,
            Decrypt = -1
        }
        public static string alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";

        public static string Vigenere(string data, string key, Operation action)
        {
            int indexOfKey = 0;
            string result = "";
            foreach (var c in data)
            {
                if (alphabet.Contains(Char.ToLower(c).ToString()))
                {
                    int offset = alphabet.IndexOf(Char.ToLower(key[indexOfKey]));
                    int res = alphabet.IndexOf(Char.ToLower(c)) + (int)action * offset;
                    res = res < 0 ? 33 + res : res % 33;
                    if (Char.IsUpper(c))
                    {
                        result += Char.ToUpper(alphabet[res]).ToString();
                    }
                    else
                    {
                        result += Char.ToLower(alphabet[res]).ToString();
                    }
                    indexOfKey = (++indexOfKey) % key.Length;
                }
                else
                {
                    result += c.ToString();
                    continue;
                }
            }
            return result;
        }
    }
}
