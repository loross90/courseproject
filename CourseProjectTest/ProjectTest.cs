﻿using System;
using CourseProject;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Microsoft.Win32;

namespace CourseProjectTest
{
    [TestClass]
    public class ProjectTest
    {
        [TestMethod]
        public void EncryptTest()
        {
            //arrange
            string initialData = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Resources\Result_v5.txt"));
            string finalData;
            //act
            finalData = Operations.Vigenere(Operations.Vigenere(initialData, "скорпион", Operations.Operation.Decrypt),
                "скорпион",Operations.Operation.Encrypt);
            //assert
            Assert.AreEqual(initialData, finalData);
        }
        [TestMethod]
        public void EncryptWithUpperCaseTest()
        {
            //arrange
            string initialData = "СлоЖноЕ ПрЕДЛоЖенИе с РазНыми РеГИстрАМИ";
            string finalData;
            //act
            finalData = Operations.Vigenere((Operations.Vigenere(initialData, "ключ", Operations.Operation.Encrypt)),
                "кЛюЧ", Operations.Operation.Decrypt);
            //assert
            Assert.AreEqual(initialData, finalData);
        }
        [TestMethod]
        public void EncryptWithStrangeSymbols()
        {
            //arrange
            string initialData = "поздравляю, ты получил исходный текст!!!";
            string finalData;
            //act
            finalData = Operations.Vigenere("бщцфаирщри, бл ячъбиуъ щбюэсяёш гфуаа!!!","скорпион",Operations.Operation.Decrypt);
            //assert
            Assert.AreEqual(initialData,finalData);
        }
        [TestMethod]
        public void EncryptWithForeignSymbols()
        {
            //arrange
            string initialData = "NowYouSeeSharp through every code because English symbols do not change. English, ... ,do you speak it?";
            string finalData;
            //assert
            finalData = Operations.Vigenere(initialData,"КлючНаРусском",Operations.Operation.Encrypt);
            //assert
            Assert.AreEqual(initialData,finalData);
        }
    }
}
